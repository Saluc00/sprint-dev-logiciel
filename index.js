const prompt = require('prompt-sync')();

class Item {
    constructor(label, quantity, price) {
        this.label = label;
        this.quantity = parseInt(quantity);
        this.price = parseInt(price);
    }

    printTotal() {
        console.log(`Le produit ${this.label} d'une quantité de ${this.quantity} d'un prix de ${this.price} coûte ${this.quantity * this.price}€`);
    }
}

const states = {
    'fr': 20,
    'en': 5,
}

let yesOrNo = true;
let reduction = 0;
let items = [];

const createItem = () => {
  let quantity = prompt('Quel est la quantité du produit ? : ');
  let price = prompt('Quel est le prix du produit ? : ');
  let label = prompt('Quel est le nom du produit ? : ');

  let item = new Item(label, quantity, price);
  items.push(item);
  item.printTotal();
}

console.log('Les taux de TVA en France sont 20%, 10%, 5,5% et 2,1%.');
console.log('Les taux de reduction sont de 3%, 5%, 7%, 10% et 15%');

let selectedState = prompt("Entrez le code pays: ");
console.log("La TVA du pays est: ", states[selectedState]);
const TVA = states[selectedState];
while(yesOrNo) {
    createItem();
    yesOrNo = prompt('Souhaité vous ajouter un autre produit ? : ');
    if(yesOrNo == "no" || yesOrNo == "non") {break;}
}

const ht = items.reduce((acc, item) => acc + (item.quantity * item.price), 0);
console.log('Prix total de la commande sans TVA: ', ht);
console.log('TVA: ', TVA,  'Coût de la TVA: ', TVA);

let valeurReduite = ht
if(ht >= 1000 && ht < 5000) { valeurReduite = ht - (ht * 3 / 100); reduction = 3}
else if(ht >= 5000 && ht < 7000) { valeurReduite = ht - (ht * 5 / 100); reduction = 5}
else if(ht >= 7000 && ht < 10000) { valeurReduite = ht - (ht * 7 / 100); reduction = 7}
else if(ht >= 10000 && ht < 15000) { valeurReduite = ht - (ht * 10 / 100); reduction = 10}
else if(ht >= 15000) { valeurReduite = ht - (ht * 15 / 100); reduction = 15}


let TTC = valeurReduite + (valeurReduite * TVA / 100)
console.log('TTC: ', TTC);
console.log("Reduction %:", reduction, "soit une reduction de:", ht * reduction / 100, "Prix avec réduction de:",  TTC, 'sans reduction:', ht + (ht * TVA / 100));