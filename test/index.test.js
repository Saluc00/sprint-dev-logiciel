import prompCreator from 'prompt-sync';

jest.mock(
  'prompt-sync',
  () => {
    const mPrompt = jest.fn();
    return jest.fn(() => mPrompt);
  },
  { virtual: true },
);

describe('index.js', () => {

  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  afterAll(() => {
    jest.resetAllMocks();
  });

  it('should pass', () => {
    jest.spyOn(process, 'exit').mockImplementation();
    const errorSpy = jest.spyOn(console, 'log').mockImplementation();
    const mPrompt = prompCreator();
    mPrompt.mockReturnValueOnce('fr');
    require('../index');


    expect(errorSpy).toBeCalledWith("Les taux de TVA en France sont 20%, 10%, 5,5% et 2,1%.");
    expect(errorSpy).toBeCalledWith("Les taux de reduction sont de 3%, 5%, 7%, 10% et 15%");
    expect(errorSpy).toBeCalledWith("La TVA du pays est: ", 20);
  });
});
